<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Buat Account Baru</h1>
    <h3>Sign Up Form</h3>
    <form action="/post" method="POST">
        @csrf
        <label>First Name:</label><br>
        <input type="text" name="firstname"><br><br>
        <label>Last Name:</label><br>
        <input type="text" name="lastname"><br>
        <p>Gender:</p> 
        <input type="radio" name="gender">Male <br>
        <input type="radio" name="gender">Female <br>
        <input type="radio" name="gender">Ora Wadon Ora Lanang<br>
        <input type="radio" name="gender">Other <br><br>
        <label>Nationality:</label>
        <select name="Kewarganegaraan">
            <option value="Indonesia">Indonesia</option>
            <option value="Singaporean">Singaporean</option>
            <option value="Malaysia">Malaysia</option>
            <option value="Australian">Australian</option>
        </select>
        <p>Language Spoken:</p>
        <input type="checkbox">Bahasa Indonesia <br>
        <input type="checkbox">English <br>
        <input type="checkbox">Arabic<br>
        <input type="checkbox">Japanese<br>
        <input type="checkbox">Other <br>
        <p>Bio</p>
        <textarea name="Bio" id="" cols="30" rows="10"></textarea><br>
        <input type="submit" value="Sign up">
</body>
</html>